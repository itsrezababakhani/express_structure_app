const shopRouter = require("./modules/shop/routes");
exports.router = app => {
  app.get("/", shopRouter);
};
